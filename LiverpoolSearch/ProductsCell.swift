//
//  ProductsCell.swift
//  LiverpoolSearch
//
//  Created by Grupo Asesores on 11/05/16.
//  Copyright © 2016 Grupo Asesores. All rights reserved.
//

import UIKit

class ProductsCell : UITableViewCell {
    
    @IBOutlet weak var titulo: UILabel!
    @IBOutlet weak var precio: UILabel!
    @IBOutlet weak var ubicacion: UILabel!
    @IBOutlet weak var imagen: UIImageView!
    
}