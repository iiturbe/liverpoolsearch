//
//  ViewController.swift
//  LiverpoolSearch
//
//  Created by Grupo Asesores on 11/05/16.
//  Copyright © 2016 Grupo Asesores. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    
    //Properties
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    var tableData = [ProductsCell]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        searchBar.delegate = self
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            let criterio = searchBar.text!
            let stringUrl = "http://www.liverpool.com.mx/tienda?s=\(criterio)&format=json"
            if let url = NSURL(string: stringUrl), let data = NSData(contentsOfURL: url) {
                do {
                    let json: NSDictionary = try NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers) as! NSDictionary
                    let content: NSArray = json.objectForKey("contents") as! NSArray
                    let mainContent: NSArray = content[0].objectForKey("mainContent") as! NSArray
                    let contents: NSArray = mainContent[2].objectForKey("contents") as! NSArray
                    let records: NSArray = contents[0].objectForKey("records") as! NSArray
                    for record in records {
                        let attributes: NSDictionary = record.objectForKey("attributes") as! NSDictionary
                        let precio = attributes.objectForKey("maximumListPrice") as! NSArray
                        let titulo = attributes.objectForKey("product.displayName") as! NSArray
                        if attributes.objectForKey("product.largeImage") != nil {
                            let imgUrl: NSArray = attributes.objectForKey("product.largeImage") as! NSArray
                            let url = NSURL(string: imgUrl[0] as! String)
                            let data = NSData(contentsOfURL: url!)
                            let imagen = UIImage(data: data!)
                            
                            let identifier = "productCell"
                            let cell = self.tableView.dequeueReusableCellWithIdentifier(identifier) as! ProductsCell
                            cell.titulo.text = titulo[0] as! String
                            cell.precio.text = "$ \(precio[0] as! String)"
                            cell.imagen.image = imagen
                            cell.ubicacion.text = "Tienda"
                            self.tableData.append(cell)
                        }
                        dispatch_async(dispatch_get_main_queue(), {
                            self.tableView.reloadData()
                        })
                        
                        //La ubicacion no me fue posible identificarla dentor de las propiedades del json
                        
                    }
                } catch {
                    NSLog(":::ERROR DE PARSIO EN JSON RESPONSE:::")
                }
            }
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let identifier = "productCell"
        var cell = tableView.dequeueReusableCellWithIdentifier(identifier) as! ProductsCell
        cell = self.tableData[indexPath.row]
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tableData.count
    }
}

